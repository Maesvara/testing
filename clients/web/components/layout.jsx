import React, { useState, useRef } from "react";
import { DarkTheme } from "./themes/dark";
import GlobalTheme from "./themes/global";
import { ThemeProvider } from "styled-components";
import { useOnClickOutside } from "../webhooks/menu";
import { Burger, Menu } from "./_exports/components";


function Layout(props) {
    const [open, setOpen] = useState(false);
    const node = useRef();

    useOnClickOutside(node, () => setOpen());
    return (
        <ThemeProvider theme={DarkTheme}>
            <>
                <GlobalTheme />
                <div ref={node}>
                    <Burger open={open} setOpen={setOpen} />
                    <Menu open={open} setOpen={setOpen} />
                </div>
                <div>{props.children}</div>
            </>
        </ThemeProvider>
    );
}

export default Layout;
