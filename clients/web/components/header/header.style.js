import styled from 'styled-components';

export const HeaderStyle = styled.div`
    width: 100%;
    height: 760px;
    background: linear-gradient(150deg, #53f 15%, #05d5ff 70%, #a6ffcb 94%);
    transform: skewY(-12deg);
    transform-origin: 0;
    position: absolute;
    top: 0;
`;

export const TextStyle = styled.div`
    position: relative;
    height: 760px;
    display: flex;
    margin: -60px 0 0 150px;
    flex-direction: column;
    justify-content: center;
    color: black;
    max-width: 1024px;
    h1 {
        font-size: 40px;
        opacity: 0;
        animation: fadein 1s;
        animation-delay: 0.3s;
        z-index:1000;
        animation-fill-mode: forwards;
    }
    p {
        color: #d9fcff;
        font-size: 20px;
        max-width: 50%;
        font-size: 17px;
        line-height: 28px;
        opacity: 0;
        animation: fadein 1s;
        animation-delay: 0.8s;
        animation-fill-mode: forwards;
    }
`;