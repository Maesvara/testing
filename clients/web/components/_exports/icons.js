export { default as Person } from "@material-ui/icons/Person";
export { default as Info } from "@material-ui/icons/Info";
export { default as Library } from "@material-ui/icons/LibraryBooks";