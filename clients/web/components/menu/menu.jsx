import { MenuStyle } from './menu.style';
import React from 'react';
import { bool } from 'prop-types';
import { Person, Info, Library } from "../_exports/icons"

const Menu = ({ open }) => {
    return (
        <MenuStyle open={open}>
            <a href="/">
                <p><Person/> <a>Home</a></p>
            </a>
            <a href="/about">
                <p><Info/> <a>About</a></p>
            </a>
            <a href="/posts">
                <p><Library/> <a>Posts</a></p>
            </a>
        </MenuStyle>
    )
};

Menu.propTypes = {
    open: bool.isRequired,
};

export default Menu;
