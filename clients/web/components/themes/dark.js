export const DarkTheme = {
    primaryDark: '#0D0C1D',
    primaryLight: '#EFFFFA',
    primaryHover: '#969ce3',
    mobile: '576px',
};