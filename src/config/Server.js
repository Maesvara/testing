import express from "express";
import bodyParser from "body-parser";
import RouteReader from "./RouteReader";
import mongoose from "mongoose";

class Server {
    constructor() {
        this.server = express();
        this.config();
        //this.initRoutes = new InitRoutes(this.server);
        this.routeReader = new RouteReader(this.server); 
        this.mongoSetup();
    }

    config() {
        this.server.use(bodyParser.json());
        this.server.use(bodyParser.urlencoded({ extended: false }));
    }

    mongoSetup() {
        const url = process.env.MONGODB_URI || `mongodb://localhost:27017/my-database`;
        mongoose.set("useNewUrlParser", true);
        mongoose.Promise = global.Promise;
        mongoose.set("useFindAndModify", false);
        mongoose.set("useCreateIndex", true);
        mongoose.set("useUnifiedTopology", true);
        mongoose.connect(url);
    }
}

export default new Server().server