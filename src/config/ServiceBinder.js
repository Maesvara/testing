import Service from './Service';

// Bind service template to the model.
// Use this if you don't need any extra functionalities besides the default already specified in Service.js
class ServiceBinder extends Service {
  constructor(model) {
    super(model);
  }
}

export default ServiceBinder;