import fs from "fs";
import path from "path";
import find from "find";

export default class Router {
    constructor(app) {
        this.readRoutes(app);
    }

    readRoutes(app) {
        const component_path = '../components';
        const base_path = path.join(__dirname, component_path);
        fs.readdir(base_path, (err, elements) => {
            if(err) return console.log(err);
            if(!elements) return console.log('No elements found.');

            elements.forEach(element => {
                let current_path = path.join(base_path, '/', element);
                fs.lstat(current_path, (err, stat) => {
                    if(err) return console.log('Error checking pathtype for %s', current_path);
                    if(stat.isDirectory()) {
                        find.file(/\Route.js$/, current_path, function(files) {
                            return require(files[0])(app);
                        })
                        return console.log('read path %s', current_path);
                    }
                }) 
            })
        })
    }
}