import Service from '../config/Service';

// Use this method if you need to add or override any functionalities. 
// Added functionalities need to be defined in your controller however.
class PostService extends Service {
  constructor(model) {
    super(model);
  }
}

export default PostService;