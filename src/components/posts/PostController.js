import Controller from "../../config/Controller";
import ServiceBinder from "../../config/ServiceBinder";
import Post from  "./Post";

const serviceBinder = new ServiceBinder(
  new Post().getInstance()
);

class PostController extends Controller {
  constructor(service) {
    super(service);
  }
}

export default new PostController(serviceBinder);