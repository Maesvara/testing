import PostController from "./PostController";

module.exports = (app) => {
  app.get(`/api/post`, PostController.getAll);
  app.get(`/api/post/:params`, PostController.get);
  app.post(`/api/post`, PostController.insert)
  app.put(`/api/post/:id`, PostController.update);
  app.delete(`/api/post/:id`, PostController.delete);
}