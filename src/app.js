import server from './config/server';
import cluster from "cluster";
require('dotenv').config();
const PORT = process.env.PORT || 5000;

if(cluster.isMaster) {
  // Count amount of cores available on current OS
  const cpu_count = require('os').cpus().length - 1;

  for(let i = 0; i < cpu_count; i++) {
    cluster.fork();
  }

  // A worker died for some reason
  cluster.on('exit', (worker) => {
    // Communist business, replace worker with a new one.
    console.log('Worked %d has died.', worker.id);
    cluster.fork();
  });
} else {

  // Spawn a new process. DO NOTE THAT THIS IS NOT GUARANTEED TO RUN ON A SEPERATE CORE. IT ONLY HAS A CHANCE TO DO SO NOW.
  server.listen(PORT, () => {
    console.log('Worker %d running!', cluster.worker.id);
  })
}