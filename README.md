# Install mongodb
1. open terminal
2. use command `brew tap mongodb/brew`. This wil enable a brew installation
3. use command `brew install mongodb-community`, prefix with `sudo` if required prompted to.
4. use command `brew services start mongodb-community` to run mongo on your machine.

# Running this project
1. `git clone https://Maesvara@bitbucket.org/Maesvara/testing.git [your_dir_name]`
2. `npm install`
3. Copy sampledotenv `cp DOTENVEXAMPLE .env` and edit the values
4. `npm run dev:start`

# Quickly adding a new component
1. `npm run generate <COMPONENT_NAME> <OPTION>`
2. use either option `default` or `custom`. None are accepted as well.