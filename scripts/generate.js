const argument = process.argv[3] || 'default',
    path = require('path'),
    fs = require('fs'),
    component_name = process.argv[2],
    execSync = require('child_process').execSync,
    types = [
        '.js',
        'Controller.js',
        'Route.js',
        'Service.js'
    ];

if (argument != 'default' && argument != 'custom') return console.log('Please either specify custom or default');
if (component_name === undefined || component_name === null) return console.log('please specify a component_name');

let multiple = component_name.endsWith('s') ? '' : 's';
let new_dir = component_name + multiple;
execSync(`mkdir ${new_dir}`, { cwd: path.join(__dirname, '../src/components') });

types.forEach((value, id, array) => {
    execSync(`touch ${capitalize(component_name) + value}`, { cwd: path.join(__dirname, '../src/components', new_dir) });
    if (id === array.length - 1) {
        let is_custom = argument === 'default' ? false : true;
        generate_default(is_custom);
    }
})

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function generate_default(is_custom) {
    fs.readdir(path.join(__dirname, '../src/components', new_dir), (err, files) => {
        writeModel(path.join(__dirname, '../src/components', new_dir, files[0]));
        writeController(path.join(__dirname, '../src/components', new_dir, files[1]), is_custom);
        writeRoute(path.join(__dirname, '../src/components', new_dir, files[2]));
        writeService(path.join(__dirname, '../src/components', new_dir, files[3]));
    })
}

function writeModel(path) {
    const template = `import mongoose, { Schema } from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

class ${capitalize(component_name)} {

  initSchema() {
    const schema = new Schema({
    //   title: {
    //     type: String,
    //     required: true,
    //   }
    }, { timestamps: true });

    schema.plugin(uniqueValidator);
    mongoose.model("${new_dir}", schema);
  }

  getInstance() {
    this.initSchema();
    return mongoose.model("${new_dir}");
  }
}

export default ${capitalize(component_name)};`

    fs.writeFile(path, template, (err) => {
        if(err) return console.log('error writing model %s', err);
        return console.log('Model written for %s', component_name)
    })
}

function writeController(path, is_custom) {
    const default_template = `import Controller from "../../config/Controller";
import ServiceBinder from "../../config/ServiceBinder";
import ${capitalize(component_name)} from  "./${capitalize(component_name)}";
    
const serviceBinder = new ServiceBinder(
    new ${capitalize(component_name)}().getInstance()
);
    
class ${capitalize(component_name)}Controller extends Controller {
    constructor(service) {
        super(service);
    }
}
    
export default new ${capitalize(component_name)}Controller(serviceBinder);
    `

    const custom_template = `import Controller from "../../config/Controller";
import ${capitalize(component_name)}Service from "./${capitalize(component_name)}Service";
import ${capitalize(component_name)} from  "./${capitalize(component_name)}";
    
const ${component_name}Service = new ${capitalize(component_name)}Service(
  new ${capitalize(component_name)}().getInstance()
);

class ${capitalize(component_name)}Controller extends Controller {
  constructor(service) {
    super(service);
  }
  // Add extra methods or override existing ones
}
    
export default new ${capitalize(component_name)}Controller(${component_name}Service);
    `

    if(is_custom) {
        fs.writeFile(path, custom_template, (err) => {
            if(err) return console.log('error writing model %s', err);
            return console.log('Custom controller written for %s', component_name)
        }) 
    } else {
        fs.writeFile(path, default_template, (err) => {
            if(err) return console.log('error writing model %s', err);
            return console.log('Default controller written for %s', component_name)
        })
    }
}

function writeRoute(path) {
    const template = `import ${capitalize(component_name)}Controller from "./${capitalize(component_name)}Controller";

module.exports = (app) => {
    app.get(\`/api/${component_name}\`, ${capitalize(component_name)}Controller.getAll);
    app.get(\`/api/${component_name}/:params\`, ${capitalize(component_name)}Controller.get);
    app.post(\`/api/${component_name}\`, ${capitalize(component_name)}Controller.insert)
    app.put(\`/api/${component_name}/:id\`, ${capitalize(component_name)}Controller.update);
    app.delete(\`/api/${component_name}/:id\`, ${capitalize(component_name)}Controller.delete);
}`

    fs.writeFile(path, template, (err) => {
        if(err) return console.log('error writing model %s', err);
        return console.log('Route written for %s', component_name)
    })
}

function writeService(path) {

    const template = `import Service from '../../config/Service';

// Use this method if you need to add or override any functionalities. 
// Added functionalities need to be defined in your controller however.
class ${capitalize(component_name)}Service extends Service {
    constructor(model) {
        super(model);
    }
}
    
export default ${capitalize(component_name)}Service;`

fs.writeFile(path, template, (err) => {
    if(err) return console.log('error writing model %s', err);
    return console.log('Service written for %s', component_name)
})
}